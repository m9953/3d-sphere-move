#ifndef CREATESKIN_HPP
#define CREATESKIN_HPP

#include "mwTPoint3d.hpp"
#include "mwDiscreteFunction.hpp"
#include "mwArcFunction.hpp"

#include <fstream>
#include <iostream>
#include <algorithm>

void CreateSkin( const cadcam::mwTPoint3d<double> refPoint, 
				const unsigned long nx, const unsigned long ny, 
				const unsigned long nz, const double sphereRad, 
				mwDiscreteFunction &func, const double deltaT, 
				const double delta, const std::string &skinFileName );


template< class T > struct line
{
	//!This struct represents a line set as a vector and a start point.
	/*!The line is an array of 2 objects: mwTVector3d and mwTPoint3d
	*/
private:
	cadcam::mwTPoint3d<T> elems[2];
public:
	//#############################################################################

	//!Sets default values for a line equation components.
	inline line()
	{
		elems[0] = cadcam::mwTVector3d<T>();
		elems[1] = mwArcFunction::point3d();
	};

	//#############################################################################

	//!Set a line.
	/*!Sets the line by 2 points.
	\param first is a first point
	\param second is a second point
	*/
	inline line(const mwArcFunction::point3d& first, const mwArcFunction::point3d& second)
	{
		elems[0] = cadcam::mwTVector3d<T>(second.x() - first.x(),
			second.y() - first.y(),
			second.z() - first.z());
		elems[1] = first;
	};

	//#############################################################################

	//!Get vector
	/*!Gets the vector of the line
	\returns constant reference to the mwTVector3d
	*/
	inline const cadcam::mwTVector3d<T> vec() const
	{
		return elems[0];
	};

	//#############################################################################

	//!Get start point
	/*!Gets the start point of the line equation.
	\returns constant reference to the point3d
	*/
	inline const mwArcFunction::point3d sPoint() const
	{
		return elems[1];
	};

};


#endif /* CREATESKIN_HPP */
