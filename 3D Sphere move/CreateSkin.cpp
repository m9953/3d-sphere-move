#define _USE_MATH_DEFINES

#include "CreateSkin.hpp"

//!Adding sector to the file.
/*!Adds end points of the stated sector to the file.
	/param refPoint is a cloud reference point at
	/param nx is a number of points in x direction
	/param ny is a number of points in y direction
	/param nz is a number of points in z direction
	/param sphereRad is a radius of the sphere
	/param func is a function object to be evaluated
	/param startT is a start parameter t of the arc function 
	/param endT is a end parameter t of the arc function 
	/param delta is a distance between points in the point grid (same for x, y and z directions)
	/param skinFileName is a name of the file to write the skin data to
	/param withLine is a condition that represent if the sphere have a path by the line, or just in the start and final positions
*/
void AddSector( const cadcam::mwTPoint3d<double> refPoint,
				const unsigned long nx, const unsigned long ny,
				const unsigned long nz, const double sphereRad,
				mwDiscreteFunction& func, double startT, double endT,
				const double delta, const std::string& skinFileName,
				bool withLine = 1 );

//!Get the distance from the point to the line.
/*!Calculates a distance from the point to the line
/param fline is a linear path of the sphere stated by vector and start point
/param point is a given point
*/
template <class T>
T distToLine( const line<T>& fLine, const mwArcFunction::point3d& point );

//!Get the distance from a point to a sphere path
/*!Calculates the distance from a point to the sphere path
/param startPoint is a position of the sphere in the start t
/param endPoint is a position of the sphere in the end t
/param point is a given point
/param sphereRad is a radius of the sphere
/param withLine is a condition that represent if the sphere have a path by the line, or just in the start and final positions

*center is a constantly set representation of the function center with coords (500, 250, 100)
*/
double dist( const mwArcFunction::point3d& startPoint,
			 const mwArcFunction::point3d& endPoint,
			 const mwArcFunction::point3d& point, double sphereRad,
			 bool withLine = 1 );

//!Check for a belonging to a sector.
/*!Checks if given point lies in a sector set by start point, center of the function, and the end point.
		/param sPoint is a position of the sphere in the start t
		/param ePoint is a position of the sphere in the end t
		/param point is a given point

		*center is set constantly as in the arc function with coordinates (500, 250, 100)
*/
template < class T >
bool isInSector( const mwArcFunction::point3d& sPoint,
				 const mwArcFunction::point3d& ePoint,
				 const cadcam::mwTPoint3d<T>& point );

//!Check of displaying point in the sector.
/*!Checks if the point should be displayed in a sector.
		/param startT is a start parameter t of the arc function 
		/param endT is an end parameter t of the arc function 
		/param sPoint is a position of the sphere in the start t
		/param ePoint is a position of the sphere in the end t
		/param point is a given point
*/
template < class T >
bool isDisplayed( const double& startT, const double& endT,
				  const mwArcFunction::point3d& sPoint,
				  const mwArcFunction::point3d& ePoint,
				  const cadcam::mwTPoint3d<T>& point );

void CreateSkin( const cadcam::mwTPoint3d<double> refPoint, 
				 const unsigned long nx, const unsigned long ny, 
				 const unsigned long nz, const double sphereRad, 
				 mwDiscreteFunction &func, const double deltaT, 
				 const double delta, const std::string &skinFileName )
{

	//Your source code here...

	//!Clearing a stored file data.
	std::fstream file;
	file.open(skinFileName, std::ios::out);
	file.close();

	//!Check if our path is a part of a circle
	if (func.GetBeginParameter() != 0 || func.GetEndParameter() != 1)
	{
		//!Add part, from end to begin parameter, without cuttin the path of the sphere on it`s way 
		AddSector(refPoint, nx, ny, nz, sphereRad, func, func.GetEndParameter(), func.GetBeginParameter(), delta, skinFileName, 0);
	}
	
	for ( double i = func.GetBeginParameter(); i < func.GetEndParameter() - deltaT; i += deltaT ) 
	{
		//!Add seector
		AddSector(refPoint, nx, ny, nz, sphereRad, func, i, i + deltaT, delta, skinFileName);
		
		//!Check if step doesn`t fit to the end parameter value
		if (i + 2*deltaT >= func.GetEndParameter())
			AddSector(refPoint, nx, ny, nz, sphereRad, func,i + deltaT, func.GetEndParameter(), delta, skinFileName, 0);
	}

}

void AddSector( const cadcam::mwTPoint3d<double> refPoint,
				const unsigned long nx, const unsigned long ny,
				const unsigned long nz, const double sphereRad, 
				mwDiscreteFunction& func, double startT, double endT,
				const double delta, const std::string& skinFileName, 
				bool withLine )
{
	//Tell information about the sector.
	std::cout << "Starting sector: (" << startT << "; " << endT << ") " << std::endl;

	//Cast func to an ArcFunction to Evaluate position of the sphere.
	mwArcFunction arcfunc = dynamic_cast<mwArcFunction&>(func);

	//Calculate a position of the sphere in a start/end t.
	const mwArcFunction::point3d startPoint = arcfunc.Evaluate(startT);
	const mwArcFunction::point3d endPoint = arcfunc.Evaluate(endT);

	//Distance from the point to the sphere path.
	double distance;

	//!Opening the file to write the skin data to.
	std::fstream file;
	file.open(skinFileName, std::ios::app);

	//Itering each point of the plane.
	for (double y = refPoint.y(); y < ny + refPoint.y(); y += delta)
	{
		for (double x = refPoint.x(); x < nx + refPoint.x(); x += delta)
		{
			//!Checking whether we should display this point or not.
			if ( !isDisplayed(startT, endT, startPoint, endPoint, cadcam::mwTPoint3d<double>(x, y, nz - refPoint.z()) ) )
				continue;

			double z = nz - refPoint.z();
			for (z; z >= refPoint.z(); z -= delta)
			{
				//!Calculate distance, depending whether we need to cut a linear path, or not.
				distance = dist(startPoint, endPoint, mwArcFunction::point3d(x, y, z), sphereRad, withLine);

				//!Go out if we are outside of a sphere path.
				if (distance > sphereRad)
					break;
			}
			
			//!Add coords to the result file list.
			file << x << " " << y << " " << z << "\n";
		}
	}

	//!Closing file.
	file.close();

	//Inform that processing of the sector is finished.
	std::cout << "Finished" << std::endl << std::endl;
	//system("pause");
	system("cls");
}

template <class T>
T distToLine( const line<T>& fLine, const mwArcFunction::point3d& point ) 
{
	cadcam::mwTVector3d<T> tmp = cadcam::mwTVector3d<T>(point - fLine.sPoint());

	tmp = fLine.vec() % tmp;
	T res = ~tmp;
	res /= ~(fLine.vec());

	return res;
}

double dist( const mwArcFunction::point3d& startPoint,
			 const mwArcFunction::point3d& endPoint,
			 const mwArcFunction::point3d& point, double sphereRad,
			 bool withLine )
{
	mwArcFunction::point3d center(500, 250, 100);
	//A line from the startPoint to the endPoint.
	line<double> fLine(startPoint, endPoint);

	if (withLine)
	{
		if (distToLine(fLine, point) <= sphereRad)
		{
			//Length of the segment stated from the startPoint to the endPoint.
			double fLineLen = ~fLine.vec();

			//Vector from the startPoint to the projection of a point on the fLine.
			mwArcFunction::point3d projection = fLine.vec() / fLineLen * (fLine.vec() * (point - startPoint) / fLineLen);

			//If projection is out of segment - calculate distance as a distance to the closest start/endPoint.
			if ( ~projection > fLineLen || ~(fLine.vec() - projection) > fLineLen )
			{
				return std::min( ~(point - startPoint),
								 ~(point - endPoint) );
			}
		}
		return distToLine(fLine, point);
	}
	return std::min( ~(point - startPoint),
					 ~(point - endPoint) );
}

template < class T >
bool isInSector( const mwArcFunction::point3d& sPoint,
				 const mwArcFunction::point3d& ePoint,
				 const cadcam::mwTPoint3d<T>& point )
{
	
	mwArcFunction::point3d center(500, 250, 100);
	mwArcFunction::point3d tmp;
	mwArcFunction::point3d arr[] = { sPoint, ePoint, point };
	double angle[3];

	for (size_t i = 0; i < 3; i++)
	{
		tmp = arr[i];
		tmp -= center;

		if (tmp.y() < 0)
		{
			angle[i] = atan2(tmp.y(), tmp.x()) + M_PI*2;
		}
		else
			angle[i] = atan2(tmp.y(), tmp.x());
	}

	double deltaSE = std::min((2 * M_PI - abs(angle[0] - angle[1])), abs(angle[0] - angle[1]));
	double deltaSP = std::min((2 * M_PI - abs(angle[0] - angle[2])), abs(angle[0] - angle[2]));
	double deltaEP = std::min((2 * M_PI - abs(angle[1] - angle[2])), abs(angle[1] - angle[2]));

	return ( deltaSE >= deltaSP && deltaSE >= deltaEP );
}

template < class T >
bool isDisplayed( const double& startT, const double& endT, 
				  const mwArcFunction::point3d& sPoint, 
				  const mwArcFunction::point3d& ePoint, 
				  const cadcam::mwTPoint3d<T>& point )
{
	if ( startT > endT )
	{
		if( !isInSector(ePoint, sPoint, point) )
			return 0;
	}
	else 
	{
		if ( !isInSector(sPoint, ePoint, point) )
			return 0;
	}
	return 1;
}
