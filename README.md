# 3D sphere move

3D sphere move - project was a test task for a job at ModuleWorks. Here we have a sphere that moves thru a 3d surface on a specific path and delete from it each point it touches. My task was to find this dots and save the 1st layer of the surface. 

NOTE: My code is stated in files CreateSkin.cpp and CreateSkin.hpp, everything else was provided by ModuleWorks as a condition of the task! (Documentation.pdf was created by myself as it stated in the condition, described in test_task.pdf)

LINK TO THE ModuleWorks website: https://www.moduleworks.com/


